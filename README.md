ADDRESS BOOK APPLICATION 

The application is a web-app that displays contacts of multiple address books.

The address books can be of any number and shows a list of contacts ordered by names.
Functionality is present to create a new contact, display the list of contacts in
a valid address book, update the contact details and delete a contact(CRUD).

Unique contacts only appearing in one address book are differentiated by mobile number.

Comments are added to make the code easy to understand


