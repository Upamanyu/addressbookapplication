package com.upamanyu.addressbook.service.impl;

import com.upamanyu.addressbook.dto.AddressBookDTO;
import com.upamanyu.addressbook.entity.AddressBook;
import com.upamanyu.addressbook.exception.AddressBookAlreadyExistsException;
import com.upamanyu.addressbook.repository.AddressBookRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class AddressBookServiceImplTest {

    @InjectMocks
    AddressBookServiceImpl addressBookServiceImpl;

    @Mock
    AddressBookRepository addressBookRepo;


    @Test
    void getBooksTest() {
        List<AddressBook> bookList = new ArrayList<>();
        AddressBook a = new AddressBook("Redmi");
        AddressBook b = new AddressBook("Lenovo");
        bookList.add(a);
        bookList.add(b);
        when(addressBookRepo.findAll()).thenReturn(bookList);
        assertEquals(bookList,addressBookServiceImpl.getBooks());
    }

    @Test
    void addNewAddressBookExceptionTest() {
        AddressBookDTO addressBookDTO = new AddressBookDTO();
        addressBookDTO.setAddressBookName("testBook");

        AddressBook addressBookFound = new AddressBook();
        addressBookFound.setAddressBookName("testBook");

        when(addressBookRepo.findByAddressBookNameIgnoreCase(addressBookDTO.getAddressBookName()))
                .thenReturn(addressBookFound);

        Assertions.assertThrows(AddressBookAlreadyExistsException.class, () -> {
            addressBookServiceImpl.addNewAddressBook(addressBookDTO);
        });
    }

        @Test
        void addNewAddressBookNoExceptionTest() {
            AddressBookDTO addressBookDTO = new AddressBookDTO();
            addressBookDTO.setAddressBookName("testBook");
            when(addressBookRepo.findByAddressBookNameIgnoreCase(addressBookDTO.getAddressBookName()))
                    .thenReturn(null);

            assertEquals("SUCCESS",addressBookServiceImpl.addNewAddressBook(addressBookDTO));
        }

    }
