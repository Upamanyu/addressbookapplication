package com.upamanyu.addressbook.service.impl;

import com.upamanyu.addressbook.dto.ContactDTO;
import com.upamanyu.addressbook.entity.Contact;
import com.upamanyu.addressbook.exception.ContactAlreadyExistsException;
import com.upamanyu.addressbook.repository.ContactRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;


import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ContactServiceImplTest {

    @InjectMocks
    ContactServiceImpl contactServiceImpl;

    @Mock
    ContactRepository contactRepo;

    @Test
    void addNewContactExceptionTest() {
        Integer id = 2;
        String contactName = "contact";
        String phoneNumber = "123456789";
        String addressBookName = "address";
        Contact foundContact = new Contact(id,contactName,phoneNumber,addressBookName);
        Optional<Contact> contactExisting = Optional.ofNullable(foundContact);
        when(contactRepo.findByNameAndAddressBookName(Mockito.anyString(),Mockito.anyString())).thenReturn(contactExisting);
        assertThrows(ContactAlreadyExistsException.class, () -> {
            contactServiceImpl.addNewContact(new ContactDTO(contactName,phoneNumber,addressBookName));
        });


    }

    @Test
    void addNewContactNoExceptionTest() {
        String contactName = "contact";
        String phoneNumber = "123456789";
        String addressBookName = "address";
        ContactDTO contactDTO = new ContactDTO(contactName,phoneNumber,addressBookName);
        Optional<Contact> contactExisting = Optional.empty();
        when(contactRepo.findByNameAndAddressBookName(Mockito.anyString(),Mockito.anyString())).thenReturn(contactExisting);
        assertEquals("SUCCESS,Contact Saved",contactServiceImpl.addNewContact(contactDTO));
    }

    @Test
    void findContactByIdTest() {
            int id=1;
            Contact contact = new Contact(id,"name","bookName","123456789");
            when(contactRepo.findByContactId(id)).thenReturn(contact);
            assertNotNull(contactServiceImpl.findContactById(id));
    }

    @Test
    void getAllContactsByAddressBookName() {
        List<Contact> contactList = new ArrayList<>();
        Contact a = new Contact(1,"a","book","123456789");
        Contact b = new Contact(2,"b","book","234567890");
        contactList.add(a);
        contactList.add(b);
        when(contactRepo.findByAddressBookName("book")).thenReturn(contactList);
        assertEquals(contactList,contactServiceImpl.getAllContactsByAddressBookName("book"));
    }

    @Test
    public void getAllUniqueContactsTest() {
        List<Contact>  contactList = new ArrayList<>();
        List<Contact>  contactList1 = new ArrayList<>();
        List<Contact>  contactList2 = new ArrayList<>();

        // add to repo call result list
        contactList.add(new Contact(1,"contact1","book1","123456789"));
        contactList.add(new Contact(2,"contact2","book2","234567890"));

        // list for each of the map values
        contactList1.add(new Contact(1,"contact1","book1","123456789"));
        contactList2.add(new Contact(2,"contact2","book2","234567890"));

        when(contactRepo.findUniqueContactForAllAddressBooks()).thenReturn(contactList);

        Map<String,List<Contact>> result = new HashMap<>();
        result.put("book1",contactList1);
        result.put("book2",contactList2);

        assertEquals(result,contactServiceImpl.getAllUniqueContacts());
    }


}