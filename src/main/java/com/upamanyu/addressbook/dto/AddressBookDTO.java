package com.upamanyu.addressbook.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
// data transfer object for address book
public class AddressBookDTO {

    private String addressBookName;
}
