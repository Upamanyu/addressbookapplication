package com.upamanyu.addressbook.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data

// data transfer object for contact
public class ContactDTO {

    private String name;
    private String phoneNumber;
    private String addressBookName;
}
