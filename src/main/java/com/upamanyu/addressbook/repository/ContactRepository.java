package com.upamanyu.addressbook.repository;

import com.upamanyu.addressbook.entity.Contact;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Repository
public interface ContactRepository extends JpaRepository<Contact, Integer> {
    @Query(value = "SELECT * FROM CONTACTS ORDER BY CONTACT_NAME ASC", nativeQuery = true)
    List<Contact> findAll();

    @Query(value = "SELECT * FROM CONTACTS WHERE CONTACT_ID=?1 ", nativeQuery = true)
    Contact findByContactId(Integer id);
    @Query(value = "SELECT * FROM CONTACTS WHERE ADDRESS_BOOK_NAME=?1 ORDER BY CONTACT_NAME ASC", nativeQuery = true)
    List<Contact> findByAddressBookName(String addressBookName);

    @Query(value = "SELECT * FROM CONTACTS WHERE ADDRESS_BOOK_NAME = ?2 AND CONTACT_NAME = ?1", nativeQuery = true)
    Optional<Contact> findByNameAndAddressBookName(String name, String addressBookName);

    @Query(value="SELECT * from CONTACTS WHERE PHONE_NUMBER IN (SELECT PHONE_NUMBER FROM CONTACTS GROUP BY PHONE_NUMBER HAVING COUNT(*)=1) order by CONTACT_NAME ASC",nativeQuery = true)
    List<Contact> findUniqueContactForAllAddressBooks();

    @Transactional
    void deleteByAddressBookName(String addressBookName);

}
