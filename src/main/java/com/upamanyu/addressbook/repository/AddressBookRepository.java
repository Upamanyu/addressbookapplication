package com.upamanyu.addressbook.repository;

import com.upamanyu.addressbook.entity.AddressBook;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.transaction.Transactional;
import java.util.List;

public interface AddressBookRepository extends JpaRepository<AddressBook, Long> {

    List<AddressBook> findAll();

    AddressBook findByAddressBookNameIgnoreCase(String name);

    @Transactional
    void deleteByAddressBookName(String name);
}
