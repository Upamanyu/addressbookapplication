package com.upamanyu.addressbook.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import java.util.Date;

@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(value = ContactAlreadyExistsException.class)
    public ResponseEntity<?> handleContactAlreadyExistsException(ContactAlreadyExistsException e, WebRequest request) {
        ErrorResponse error = new ErrorResponse(new Date(), e.getMessage(), request.getDescription(false));
        return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = AddressBookAlreadyExistsException.class)
    public ResponseEntity<?> handleAddressBookExistsException(AddressBookAlreadyExistsException e, WebRequest request) {
        ErrorResponse error = new ErrorResponse(new Date(), e.getMessage(), request.getDescription(false));
        return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
    }


}
