package com.upamanyu.addressbook.exception;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class AddressBookAlreadyExistsException extends RuntimeException {
    private String message;
}
