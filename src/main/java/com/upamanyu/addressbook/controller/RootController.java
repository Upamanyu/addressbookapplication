package com.upamanyu.addressbook.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class RootController {

    // to redirect to root
    @GetMapping("/")
    public String redirectHome() {
        return "redirect:/user/v1/get-all-books";
    }

}
