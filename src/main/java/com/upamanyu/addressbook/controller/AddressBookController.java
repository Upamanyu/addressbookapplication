package com.upamanyu.addressbook.controller;

import com.upamanyu.addressbook.dto.AddressBookDTO;
import com.upamanyu.addressbook.dto.ContactDTO;
import com.upamanyu.addressbook.entity.Contact;
import com.upamanyu.addressbook.service.AddressBookService;
import com.upamanyu.addressbook.service.ContactService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/user/v1")
public class AddressBookController {
    @Autowired
    private AddressBookService addressBookService;
    @Autowired
    private ContactService contactService;

    @GetMapping("/")
    public String redirectHome() {

        return "redirect:/user/v1/get-all-books";
    }

    // homepage
    @GetMapping("/get-all-books")
    public String showAllBooks(Model model) {

        model.addAttribute("addressBookList",addressBookService.getBooks());
        return "index";
    }

    // receives data from the form to add a new book
    @PostMapping("/add-new-book")
    public String addNewBook(@ModelAttribute("addressBookDTO") AddressBookDTO addressBookDTO) {

        String response = addressBookService.addNewAddressBook(addressBookDTO);
        // redirects to homepage
        return "redirect:/user/v1/get-all-books";
    }


    @PostMapping("/add-new-contact")
    public String addNewContact(@ModelAttribute("contactDTO") ContactDTO contactDTO) {
        String response = contactService.addNewContact(contactDTO);

        // redirect to the get all contacts by bookname page
        return "redirect:/user/v1/get-all-contacts-bookname/" +contactDTO.getAddressBookName();
    }

    // controller to edit a contact
    @PostMapping("/edit-contact/{id}")
    public String editContact(@PathVariable("id") Integer id,@ModelAttribute("contactDetails") ContactDTO contactDTO) {

        contactService.updateContact(id,contactDTO);

        // redirect to the get all contacts by bookname page
        return "redirect:/user/v1/get-all-contacts-bookname/" +contactDTO.getAddressBookName();
    }

    // returns form to edit contact with data prepopulated
    @GetMapping("/edit-contact-form/{id}")
    public String showEditContactForm(@PathVariable("id") Integer id,Model model) {
        Contact contactToUpdate = contactService.findContactById(id);
        ContactDTO newContactDetails = new ContactDTO(contactToUpdate.getName()
        ,contactToUpdate.getPhoneNumber(),contactToUpdate.getAddressBookName());
        model.addAttribute("id",id);
        model.addAttribute("contactDetails",newContactDetails);

        return "edit-contact";
    }


    // returns contacts by bookname
    @GetMapping("/get-all-contacts-bookname/{book}")
    public String getAllContactsByAddressBookName(@PathVariable("book") String addressBookName, Model model) {
        List<Contact> contactList = contactService.getAllContactsByAddressBookName(addressBookName);
        model.addAttribute("contactList", contactList);
        model.addAttribute("bookName",addressBookName);
        return "get-all-contacts-bookname";
    }

    // display form to add a contact
    @GetMapping("/show-add-contact-form")
    public String showAddContactForm(Model model) {
        ContactDTO contactDTO = new ContactDTO();
        model.addAttribute("addressBookList",addressBookService.getBooks());
        model.addAttribute("contactDTO", contactDTO);
        return "add-contact-form";
    }

    // add a new address book
    @GetMapping("/add-addressbook")
    public String showAddressBookForm(Model model) {
        AddressBookDTO addressBookDTO = new AddressBookDTO();
        model.addAttribute("addressBookDTO", addressBookDTO);
        return "add-address-book-form";
    }

    // get the contacts(by mobile number) which belong to only one address book
    @GetMapping("/get-all-unique-relative-contacts")
    public String getAllUniqueContacts(Model model) {
        // have assumed that contacts with same name and different mobile numbers can exist
        // so unique contact is determined by number
        Map<String,List<Contact>> uniqueContacts = contactService.getAllUniqueContacts();
        model.addAttribute("uniqueContacts", uniqueContacts);
        return "get-all-unique-relative-contacts";
    }

    // delete an address book, along with all the contacts in it
    @GetMapping("delete-book/{bookName}")
    public String deleteContact(@PathVariable("bookName") String addressBookName) {
        addressBookService.deleteAddressBook(addressBookName);
        contactService.deleteContactsInAddressBook(addressBookName);
        return "redirect:/user/v1/get-all-books";
    }


    // delete a contact from the address book
    @GetMapping("/delete-contact/{id}")
    public String deleteContact(@PathVariable("id") Integer id) {

        String bookName = contactService.deleteContact(id);
        return "redirect:/user/v1/get-all-contacts-bookname/" +bookName;
    }

}
