package com.upamanyu.addressbook.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Objects;


@Entity
@Table(name = "CONTACTS")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Contact {
    @Id
    @Column(name = "CONTACT_ID")
    private Integer id;
    @Column(name = "CONTACT_NAME")
    private String name;

    @Column(name = "ADDRESS_BOOK_NAME")
    private String addressBookName;

    @Column(name = "PHONE_NUMBER")
    private String phoneNumber;


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Contact that = (Contact) o;
        return phoneNumber.equals(that.getPhoneNumber()) && addressBookName.equals(that.getAddressBookName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(phoneNumber);
    }
}
