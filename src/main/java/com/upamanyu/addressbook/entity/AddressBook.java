package com.upamanyu.addressbook.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Objects;


@Entity
@Table(name = "ADDRESS_BOOKS")
@Data
@NoArgsConstructor
public class AddressBook {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="ADDRESS_BOOK_ID")
    private int id;
    @Column(name = "BOOK_NAME")
    private String addressBookName;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AddressBook that = (AddressBook) o;
        return addressBookName.equalsIgnoreCase(that.getAddressBookName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(addressBookName);
    }

    public AddressBook(String name) {
        this.addressBookName=name;
    }
}
