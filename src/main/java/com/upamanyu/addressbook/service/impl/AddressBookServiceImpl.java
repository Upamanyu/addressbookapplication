package com.upamanyu.addressbook.service.impl;

import com.upamanyu.addressbook.dto.AddressBookDTO;
import com.upamanyu.addressbook.entity.AddressBook;
import com.upamanyu.addressbook.exception.AddressBookAlreadyExistsException;
import com.upamanyu.addressbook.repository.AddressBookRepository;
import com.upamanyu.addressbook.service.AddressBookService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@Slf4j
@Service
public class AddressBookServiceImpl implements AddressBookService {

    @Autowired
    AddressBookRepository addressBookRepo;

    // get list of address book to render on screen
    public List<AddressBook> getBooks() {
        return addressBookRepo.findAll();
    }

    // delete address book
    public void deleteAddressBook(String name) {
        addressBookRepo.deleteByAddressBookName(name);
    }


    public String addNewAddressBook(AddressBookDTO addressBookDTO)  {

        AddressBook addressBookExisting = addressBookRepo.findByAddressBookNameIgnoreCase(addressBookDTO.getAddressBookName());

        if (Objects.isNull(addressBookExisting)) {
            AddressBook addressBook = new AddressBook(addressBookDTO.getAddressBookName());
            addressBookRepo.save(addressBook);
            log.info("New address book added!");
            return "SUCCESS";
        } else {
            throw new AddressBookAlreadyExistsException("Address book with name "+addressBookDTO.getAddressBookName()+ " already exists");
        }
    }
}
