package com.upamanyu.addressbook.service.impl;

import com.upamanyu.addressbook.dto.ContactDTO;
import com.upamanyu.addressbook.entity.Contact;
import com.upamanyu.addressbook.exception.ContactAlreadyExistsException;
import com.upamanyu.addressbook.repository.ContactRepository;
import com.upamanyu.addressbook.service.ContactService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;


@Service
public class ContactServiceImpl implements ContactService {

    private static final Logger log = LoggerFactory.getLogger(ContactServiceImpl.class);
    @Autowired
    private ContactRepository contactRepository;

    public int getMaxId() {
        return contactRepository.findAll().size() + 1;
    }

    public void updateContact(Integer id,ContactDTO contactDTO) {
        Contact contact = new Contact();
        contact.setId(id);
        contact.setAddressBookName(contactDTO.getAddressBookName());
        contact.setName(contactDTO.getName());
        contact.setPhoneNumber(contactDTO.getPhoneNumber());
        contactRepository.save(contact);
    }

    public String addNewContact(ContactDTO contactDTO) {
        Optional<Contact> contactExisting = contactRepository.findByNameAndAddressBookName(contactDTO.getName(), contactDTO.getAddressBookName());
        if (Optional.empty().equals(contactExisting)) {
            Contact contact = new Contact();
            contact.setId(getMaxId());
            contact.setAddressBookName(contactDTO.getAddressBookName());
            contact.setName(contactDTO.getName());
            contact.setPhoneNumber(contactDTO.getPhoneNumber());
            contactRepository.save(contact);
            log.info("Contact Saved Successfully!");
            return "SUCCESS,Contact Saved";
        } else {
            throw new ContactAlreadyExistsException("Contact already present in AddressBook");
        }
    }

    public Contact findContactById(Integer id) {
        return contactRepository.findByContactId(id);
    }

    public List<Contact> getAllContactsByAddressBookName(String addressBookName) {
            List<Contact> contactList = contactRepository.findByAddressBookName(addressBookName);
            log.info("Fetching contacts from DB by addressbook name!");
            return contactList;
    }


    public Map<String,List<Contact>> getAllUniqueContacts() {
        List<Contact>  contactList = contactRepository.findUniqueContactForAllAddressBooks();
        Map<String,List<Contact>> result = new HashMap<>();
        for(Contact contact: contactList) {
            List<Contact> currentList = result.get(contact.getAddressBookName());
            if(Objects.isNull(currentList)) currentList = new ArrayList<>();
            currentList.add(contact);
            result.put(contact.getAddressBookName(),currentList);
        }

        return result;
    }

    public String deleteContact(Integer id) {
        Contact contact = contactRepository.findByContactId(id);
        contactRepository.delete(contact);
        return contact.getAddressBookName();
    }

    public void deleteContactsInAddressBook(String addressbookName) {
        contactRepository.deleteByAddressBookName(addressbookName);
    }
}



