package com.upamanyu.addressbook.service;

import com.upamanyu.addressbook.dto.AddressBookDTO;
import com.upamanyu.addressbook.entity.AddressBook;

import java.util.List;

public interface AddressBookService {
    List<AddressBook> getBooks();

    public void deleteAddressBook(String name);
    public String addNewAddressBook(AddressBookDTO addressBookDTO);
}
