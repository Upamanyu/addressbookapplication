package com.upamanyu.addressbook.service;

import com.upamanyu.addressbook.dto.ContactDTO;
import com.upamanyu.addressbook.entity.Contact;

import java.util.List;
import java.util.Map;

public interface ContactService {

    public void updateContact(Integer id, ContactDTO contactDTO);
    public String addNewContact(ContactDTO contactDTO);

    public Contact findContactById(Integer id);

    public List<Contact> getAllContactsByAddressBookName(String addressBookName);

    public Map<String,List<Contact>> getAllUniqueContacts();

    public String deleteContact(Integer id);

    void deleteContactsInAddressBook(String addressbookName);
}
